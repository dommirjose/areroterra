var map;

// inicializamos el mapa en Buenos Aires
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {
      lat: -34.603722,
      lng: -58.381592
    },
    zoom: 3
  });
}

// generamos la ubicación random
function generatePositionRandom() {
  var min = -90;
  var max = 90;
  var lat = Math.random() * (+max - +min) + +min;
  var lng = Math.random() * (+max - +min) + +min;

  var myLatLng = {
    lat: lat,
    lng: lng
  };

  return (myLatLng);
}

// funcion para añadir marcadores
function addMarker(e) {
  e.preventDefault();

  let description = document.getElementById("description").value;
  let myLatLng = generatePositionRandom();
  let marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: description
  });

  // Contenido del Popup custom
  let contentString = '<div>' +
    '<h5 class="card-title">' + description + '</h5>' +
    '<p class="card-text">Latitud: ' + myLatLng.lat + '</p>' +
    '<p class="card-text">Latitud: ' + myLatLng.lng + '</p>' +
    '</div>';


  // Creamos el infoWindow que es el popup de google maps añadiendole el contenido custom
  let infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  // levantamos el popup haciendo click en el marcador
  marker.addListener('click', function () {
    infowindow.open(map, marker);
  });
}